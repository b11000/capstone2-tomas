const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
// Routes
const userRoutes = require('./routes/userRoute');
const productRoutes = require('./routes/productRoute');
const orderRoutes = require('./routes/orderRoute');
// Server Setup
const app = express();
const port = 4000;
// Connection
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));
// Routes
app.use('/api/users', userRoutes);
app.use('/api/products', productRoutes);
app.use('/api/orders', orderRoutes);
// Database
mongoose.connect("mongodb+srv://admin:admin@cluster0.gguoi.mongodb.net/ecommerce-api", {
    useNewUrlParser: true,
    useUnifiedTopology: true
});
// Run message
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database."));
// Environment Cloud ex: heroku
app.listen(process.env.PORT || port, () => console.log(`API is now online on ${process.env.PORT || port}`));