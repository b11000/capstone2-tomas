const mongoose = require("mongoose");
const orderSchema = new mongoose.Schema({
    totalAmount: {
        type: Number,
        required: [true, "Total amount is required"]
    },
    purchasedOn: {
        type: Date,
        default: new Date()
    },
    userId: {
        type: String,
        required: [true, "userId is required"]
    },
    productId: {
        type: String,
        required: [true, "Product ID is required"]
    },
    price: {
        type: Number,
        required: [true, "Price is required"]
    },
    quantity: {
        type: Number,
        default: 1
    }

});

module.exports = mongoose.model('Order', orderSchema);