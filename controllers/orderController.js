const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order');
const auth = require('../auth');
// GET user's order/s
module.exports.getUserOrder = (reqBody, userData) => {
    return User.findById(userData.userId).then(result => {
        if (userData.userId !== reqBody.userId) {
            return 'You cannot access this page'
        } else {
            return Order.findById(reqBody.orderId).then(result => {
                if (result !== null) {
                    return result
                } else {
                    return 'This order does not exist'
                }
            })
        }
    })
}
// GET all orders
module.exports.getAllOrders = () => {
    return Order.find().then((result, err) => {
        if (err) {
            return false;
        } else {
            return result;
        }
    })
};
// Create an order checkout
module.exports.createOrder = (reqBody, userData) => {
    return User.findById(userData.userId).then(result => {
        if (userData.userId == reqBody.userId) {
            return Product.findById(reqBody.productId).then(result => {
                if (result !== null) {
                    let newOrder = new Order({
                        totalAmount: `${reqBody.quantity * reqBody.price}`,
                        userId: reqBody.userId,
                        productId: reqBody.productId,
                        price: reqBody.price,
                        quantity: reqBody.quantity
                    })
                    return newOrder.save().then((order, error) => {
                        if (error) {
                            return false
                        } else {
                            return "Order creation successful"
                        }
                    });
                } else {
                    return 'Product not found'
                }
            });
        } else {
            return `You need to login to order`
        }
    });
};