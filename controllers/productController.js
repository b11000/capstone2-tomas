const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order');
const bcrypt = require('bcrypt');
const auth = require('../auth');
// Retrieve all product
module.exports.getAllProduct = () => {
	return Product.find({}).then(result => {
		return result;
	})
};
// Retrieve all active product
module.exports.getAllActive = () => {
	return Product.find({
		isActive: true
	}).then(result => {
		return result;
	})
};
// Retrieve specific product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
};
// Create product
module.exports.addProduct = (reqBody, userData) => {
	return Product.findById(userData.userId).then(result => {
		if (userData.isAdmin == false) {
			return "You're not an Admin"
		} else {
			let newProduct = new Product({
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price
			})
			return newProduct.save().then((product, error) => {
				if (error) {
					return false;
				} else {
					return "Product creation successful";
				}
			})
		}
	})
};
// Update a product
module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	return Product.findByIdAndUpdate(reqParams.ProductId, updatedProduct)
		.then((product, error) => {
			if (error) {
				return false;
			} else {
				return "Product updated Successfully";
			}
		})
};
// Archive a product
module.exports.archiveProduct = (reqParams, reqBody) => {
	let archiveProduct = {
		isActive: false
	}
	return Product.findByIdAndUpdate(reqParams.productId, archiveProduct)
		.then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		})
};