const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order');
const bcrypt = require('bcrypt');
const auth = require('../auth');
// Get all user
module.exports.getAllUser = (reqBody) => {
	return User.find({}).then(result => {
		return result;
	})
};
// Verify email 
module.exports.checkEmailExists = (reqBody) => {
	return User.find({
		email: reqBody.email
	}).then(result => {
		if (result.length > 0) {
			return `${reqBody.email} is a registered user!!`;
		} else {
			return `${reqBody.email} is not a registered user!!`;
		}
	})
};
// Update a user
module.exports.updateUser = (reqParams, reqBody) => {
	let updatedUser = {
		email: reqBody.email,
		password: reqBody.password,
		isAdmin: reqBody.isAdmin
	}
	return User.findByIdAndUpdate(reqParams.userId, updatedUser)
		.then((user, error) => {
			if (error) {
				return false;
			} else {
				return "User updated Successfully";
			}
		})
};
// Register User
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	})
};
// Login User
module.exports.loginUser = (reqBody) => {
	return User.findOne({
		email: reqBody.email
	}).then(result => {
		if (result == null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if (isPasswordCorrect) {
				return {
					accessToken: auth.createAccessToken(result.toObject())
				}
			} else {
				return false
			}
		}
	})
};