const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderController')
const auth = require('../auth');
// Route for GET auth user's order/s http://localhost:4000/api/orders/:id

router.get("/:id", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    orderController.getUserOrder(req.body, {
        userId: userData.id
    }).then(result => res.send(result))
})

// Route for GET all orders from all users http://localhost:4000/api/orders/showOrders
router.get('/showOrders', auth.verify, (req, res) => {
    orderController.getAllOrders().then(result => res.send(result))
});
// Route for creating orders(CHECK OUT) http://localhost:4000/api/orders/checkout
router.post('/checkout', auth.verify, (req, res, ) => {
    const userData = auth.decode(req.headers.authorization);
    orderController.createOrder(req.body, {
        userId: userData.id,
        purchase: userData.purchase
    }).then(result => res.send(result))
})
module.exports = router;