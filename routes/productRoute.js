const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require('../auth');
// Retrieve all products http://localhost:4000/api/products/showProducts
router.get("/showproducts", (req, res) => {
	productController.getAllProduct().then(resultFromController => res.send(resultFromController));
});
// Retrieve all active products http://localhost:4000/api/products
router.get("/", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});
// Retrieve a specific product http://localhost:4000/api/products/:productId
router.get("/:productId", (req, res) => {
	console.log(req.params.courseId);
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});
// Route for creating a product http://localhost:4000/api/products/create
router.post("/create", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	productController.addProduct(req.body, {
		userId: userData.id,
		isAdmin: userData.isAdmin
	}).then(resultFromController => res.send(resultFromController))
});
// Route for Updating a product http://localhost:4000/api/products/:id
router.put("/:productId", auth.verify, (req, res) => {
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
});
// Route for Archiving a product http://localhost:4000/api/products/:id/archive
router.put("/:productId/archive", auth.verify, (req, res) => {
	productController.archiveProduct(req.params, req.body).then(result => res.send(result))
});
module.exports = router;