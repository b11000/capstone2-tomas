const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController')
const auth = require('../auth');
// GET all users http://localhost:4000/api/users/showUsers
router.get('/showUsers', (req, res) => {
	userController.getAllUser().then(result => res.send(result))
});
// Route for updating a user http://localhost:4000/api/users/:id
router.put("/:userId", auth.verify, (req, res) => {
	userController.updateUser(req.params, req.body).then(resultFromController => res.send(resultFromController))
});
// Route for user registration http://localhost:4000/api/users/register
router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result))
});
// Route for email checking http://localhost:4000/api/users/showEmail
router.post('/checkEmail', (req, res) => {
	userController.checkEmailExists(req.body).then(result => res.send(result))
});
//Routes for authenticating a user http://localhost:4000/api/users/login
router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result))
});
module.exports = router;